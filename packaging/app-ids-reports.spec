
Name: app-ids-reports
Epoch: 1
Version: 1.0.0
Release: 1%{dist}
Summary: IDS/IPS Reports
License: GPLv3
Group: ClearOS/Apps
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base

%description
Reports for your intrusion detection/detection system.

%package core
Summary: IDS/IPS Reports - Core
License: LGPLv3
Group: ClearOS/Libraries
Requires: app-base-core
Requires: app-system-database-core
Requires: app-intrusion-detection-core
Requires: barnyard2

%description core
Reports for your intrusion detection/detection system.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/ids_reports
cp -r * %{buildroot}/usr/clearos/apps/ids_reports/

install -d -m 770 %{buildroot}/var/clearos/ids_reports

%post
logger -p local6.notice -t installer 'app-ids-reports - installing'

%post core
logger -p local6.notice -t installer 'app-ids-reports-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/ids_reports/deploy/install ] && /usr/clearos/apps/ids_reports/deploy/install
fi

[ -x /usr/clearos/apps/ids_reports/deploy/upgrade ] && /usr/clearos/apps/ids_reports/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ids-reports - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ids-reports-core - uninstalling'
    [ -x /usr/clearos/apps/ids_reports/deploy/uninstall ] && /usr/clearos/apps/ids_reports/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/ids_reports/controllers
/usr/clearos/apps/ids_reports/htdocs
/usr/clearos/apps/ids_reports/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/ids_reports/packaging
%exclude /usr/clearos/apps/ids_reports/unify.json
%dir /usr/clearos/apps/ids_reports
%dir %attr(770,webconfig,webconfig) /var/clearos/ids_reports
/usr/clearos/apps/ids_reports/deploy
/usr/clearos/apps/ids_reports/language
/usr/clearos/apps/ids_reports/libraries
