<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'ids_reports';
$app['version'] = '1.0.0';
$app['release'] = '1';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('ids_reports_app_description');
$app['powered_by'] = array(
    'vendor' => NULL,
    'packages' => array(
    ),
);


/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('ids_reports_app_name');
$app['category'] = lang('base_category_reports');
$app['subcategory'] = lang('base_subcategory_performance_and_resources');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-system-database-core',
    'app-intrusion-detection-core',
    'barnyard2',
);

$app['core_file_manifest'] = array(
);

$app['core_directory_manifest'] = array(
    '/var/clearos/ids_reports' => array('mode' => '770', 'owner' => 'webconfig', 'group' => 'webconfig'),
);


$app['delete_dependency'] = array(
    'app-ids_reports-core',
    'barnyard2',
);
